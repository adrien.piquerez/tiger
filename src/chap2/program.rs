use std::time::Instant;
use crate::tiger::tests;
use crate::tiger::lexer::Lexer;

pub fn execute() -> () {
    for (name, test) in tests::ALL_TESTS {
        let start = Instant::now();
        let lexer = Lexer::new(test);
        let tokens = lexer
            .map(|r| r.map(|t| t.0))
            .collect::<Vec<_>>();
        println!("{}: lexing {} tokens in {:?}", name, tokens.len(), start.elapsed());
    }
}