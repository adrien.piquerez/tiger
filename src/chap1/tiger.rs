pub mod syntax {
    pub enum Stm<'a> {
        Coumpound(Box<Stm<'a>>,Box<Stm<'a>>),
        Assign(&'a str, Exp<'a>),
        Print(ExpList<'a>)
    }

    pub enum Exp<'a> {
        Id(&'a str),
        Num(i32),
        Op(Box<Exp<'a>>, Binop, Box<Exp<'a>>),
        Eseq(Box<Stm<'a>>, Box<Exp<'a>>)
    }

    pub enum Binop {
        Plus
    }

    pub enum ExpList<'a> {
        List(Exp<'a>,  Box<ExpList<'a>>),
        Empty
    }
}
