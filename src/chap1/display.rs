use std::fmt::Display;
use std::fmt::Formatter;
use std::fmt::Error;
use super::tiger::syntax::{Stm, Exp, ExpList, Binop};
use super::tiger::syntax::Stm::{Coumpound, Assign, Print};
use super::tiger::syntax::Exp::{Id, Num, Op, Eseq};
use super::tiger::syntax::ExpList::{List, Empty};
use super::tiger::syntax::Binop::{Plus};
use super::tree::Tree;
use super::tree::Tree::{Leaf, Node};


impl<'a> Display for Stm<'a> {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        match self {
            Coumpound(lhs, rhs) => write!(f, "{};\n{}", lhs, rhs)?,
            Assign(id, exp) => write!(f, "{} = {}", id, exp)?,
            Print(list) => write!(f, "print({})", list)?
        }
        Ok(())
    }
}

impl<'a> Display for Exp<'a> {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        match self {
            Id(id) => write!(f, "{}", id)?,
            Num(n) => write!(f, "{}", n)?,
            Op(lhs, op, rhs) => write!(f, "{} {} {}", lhs, op, rhs)?,
            Eseq(stm, expr) => write!(f, "({}, {})", stm, expr)?
        }
        Ok(())
    }
}

impl Display for Binop {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        match self {
            Plus => write!(f, "+")?
        }
        Ok(())
    }
}

impl<'a> Display for ExpList<'a> {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        match self {
            List(head, tail) => write!(f, "{}, {}", head, tail),
            Empty => write!(f, "()")
        }
    }
}

impl<K: Display, V: Display> Display for Tree<K, V> {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        match self {
            Leaf => write!(f, "()"),
            Node(left_tree, root, value, right_tree) =>
                write!(f, "({}, ({}, {}), {})", left_tree, root, value, right_tree)
        }
    }
}
