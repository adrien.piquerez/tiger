use super::tiger::syntax::Stm::{Coumpound, Assign, Print};
use super::tiger::syntax::Exp::{Id, Num, Op, Eseq};
use super::tiger::syntax::ExpList::{List, Empty};
use super::tiger::syntax::Binop::{Plus};
use super::tree::Tree::Leaf;

pub fn execute() -> () {
    let assign = Assign("i", Eseq(Box::new(Print(List(Num(42), Box::new(Empty)))), Box::new(Op(Box::new(Num(5)), Plus, Box::new(Num(4))))));
    let print = Print(List(Id("i"), Box::new(List(Num(5), Box::new(Empty)))));
    let program = Coumpound(Box::new(assign), Box::new(print));
    println!("Program:");
    println!("{}", program);
    println!();
    println!("Max print args: {}", program.count_max_print_args());
    println!();
    println!("Result:");
    match program.interpret() {
        Ok(()) => (),
        Err(msg) => eprintln!("{}", msg)
    }

    println!();
    let tree = Leaf;
    let tree = tree
        .insert(5, "five".to_owned())
        .insert(-1, "minus one".to_owned())
        .insert(10, "ten".to_owned())
        .insert(12, "twelve".to_owned())
        .insert(9, "nine".to_owned());
    println!("Tree:");
    println!("{}", tree);
    println!();
    println!("contains 9: {}", tree.member(5));
    println!("Contains 8: {}", tree.member(8));
    println!("Lookup -1: {}", tree.lookup(-1).unwrap_or(&"None".to_owned()));
    println!("Lookup 13: {}", tree.lookup(13).unwrap_or(&"NA".to_owned()));
}