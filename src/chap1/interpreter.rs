use super::tiger::syntax::{Stm, Exp, ExpList, Binop};
use super::tiger::syntax::Stm::{Coumpound, Assign, Print};
use super::tiger::syntax::Exp::{Id, Num, Op, Eseq};
use super::tiger::syntax::ExpList::{List, Empty};
use super::tiger::syntax::Binop::{Plus};
use std::cmp::Ord;
use im::HashMap;
use im::Vector;

impl<'a> Stm<'a> {
    pub fn count_max_print_args(&self) -> usize {
        match self {
            Coumpound(lhs, rhs) =>
                lhs.count_max_print_args().max(rhs.count_max_print_args()),
            Assign(_, value) => value.count_max_print_args(),
            Print(list) => list.count().max(list.count_max_print_args())
        }
    }

    pub fn interpret(&self) -> Result<(), String> {
        let table = HashMap::<&str, i32>::new();
        let _ = self.interpret_with(&table)?;
        Ok(())
    }

    fn interpret_with(&self, table: &HashMap<&'a str, i32>) -> Result<HashMap<&'a str, i32>, String> {
        match self {
            Coumpound(lhs,rhs) => {
                let table = lhs.interpret_with(table)?;
                let table = rhs.interpret_with(&table)?;
                Ok(table)
            },
            Assign(id, value) => {
                let result = value.interpret_with(&table)?;
                Ok(table.update(id, result))
            },
            Print(list) => {
                let results = list.interpret_with(&table)?;
                let joined_results = results.iter().map(|i| format!("{}", i))
                    .collect::<Vec<String>>().join(", ");
                println!("{}", joined_results);
                Ok(table.clone())
            }
        }
    }
}

impl<'a> Exp<'a> {
    fn count_max_print_args(&self) -> usize {
        match self {
            Eseq(stm, exp) =>
                stm.count_max_print_args().max(exp.count_max_print_args()),
            _ => 0
        }
    }

    fn interpret_with(&self, table: &HashMap<&'a str, i32>)
        -> Result<i32, String> {
        match self {
            Id(id) => {
                let value = table.get(id).ok_or(format!("unknown id {}", id))?;
                Ok(value.clone())
            },
            Num(value) => Ok(value.clone()),
            Op(lhs, op, rhs) => {
                let left_hand_value = lhs.interpret_with(table)?;
                let right_hand_value = rhs.interpret_with(table)?;
                let value = op.compute(left_hand_value, right_hand_value);
                Ok(value.clone())
            },
            Eseq(stm, exp) => {
                let table = stm.interpret_with(table)?;
                let value  = exp.interpret_with(&table)?;
                Ok(value.clone())
            }
        }
    }
}

impl Binop {
    fn compute(&self, lhs: i32, rhs: i32) -> i32 {
        match self {
            Plus => lhs + rhs
        }
    }
}

impl<'a> ExpList<'a> {
    fn count(&self) -> usize {
        match self {
            List(_, tail) => 1 + tail.count(),
            _ => 0
        }
    }

    fn count_max_print_args(&self) -> usize {
        match self {
            List(head, tail) =>
                head.count_max_print_args().max(tail.count_max_print_args()),
            _ => 0
        }
    }

    fn interpret_with(&self, table: &HashMap<&'a str, i32>) -> Result<Vector<i32>, String> {
        match self {
            List(head, tail) => {
                let head_result = head.interpret_with(table)?;
                let mut results = tail.interpret_with(table)?;
                results.push_front(head_result);
                Ok(results)
            }
            Empty => Ok(Vector::<i32>::new())
        }
    }
}