pub enum Tree<K, V> {
    Leaf,
    Node(Box<Tree<K, V>>, K, V, Box<Tree<K, V>>)
}

impl<K: Ord + Eq, V> Tree<K, V> {
    pub fn insert(self, key: K, value: V) -> Self {
        match self {
            Tree::Leaf => Tree::Node(Box::new(Tree::Leaf), key, value, Box::new(Tree::Leaf)),
            Tree::Node(left_tree, root, root_value, right_tree) => {
                if key < root {
                    Tree::Node(Box::new(left_tree.insert(key, value)), root, root_value,right_tree)
                } else if key > root {
                    Tree::Node(left_tree, root, root_value, Box::new(right_tree.insert(key, value)))
                } else {
                    Tree::Node(left_tree, root, root_value, right_tree)
                }
            }
        }
    }

    pub fn member(&self, key: K) -> bool {
        match self {
            Tree::Node(_, root, _, _) if key == *root => true,
            Tree::Node(ref left_tree, root,_ , _) if key < *root => left_tree.member(key),
            Tree::Node(_, root, _, ref right_tree) if key < *root => right_tree.member(key),
            _ => false
        }
    }

    pub fn lookup(&self, key: K) -> Option<&V> {
        match self {
            Tree::Node(_, root, value, _) if key == *root => Some(value),
            Tree::Node(ref left_tree, root, _, _) if key < *root => left_tree.lookup(key),
            Tree::Node(_, root,_, ref right_tree) if key < *root => right_tree.lookup(key),
            _ => None
        }
    }
}
