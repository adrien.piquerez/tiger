use crate::tiger::{tests, parser};
use std::time::Instant;
use crate::tiger::lexer::Lexer;
use crate::tiger::analyser;

pub fn execute() {
    println!("Semantic Analysis:");
    for (name, test) in tests::ALL_TESTS {
        let start = Instant::now();
        let lexer = Lexer::new(test);
        let result = parser::parse(lexer)
            .and_then(|exp| analyser::analyse(&exp));
        match result {
            Err(e) => println!("Error while analysing {}: {:?}", name, e),
            Ok(_exp) => println!("analysed {} in {:?}", name, start.elapsed())
        }
    }
}