pub static MERGE: &str = include_str!("merge.tig");
pub static QUEENS: &str = include_str!("queens.tig");

pub static TEST1: &str = include_str!("test1.tig");
pub static TEST2: &str = include_str!("test2.tig");
pub static TEST3: &str = include_str!("test3.tig");
pub static TEST4: &str = include_str!("test4.tig");
pub static TEST5: &str = include_str!("test5.tig");
pub static TEST6: &str = include_str!("test6.tig");
pub static TEST7: &str = include_str!("test7.tig");
pub static TEST8: &str = include_str!("test8.tig");
pub static TEST9: &str = include_str!("test9.tig");

pub static TEST10: &str = include_str!("test10.tig");
pub static TEST11: &str = include_str!("test11.tig");
pub static TEST12: &str = include_str!("test12.tig");
pub static TEST13: &str = include_str!("test13.tig");
pub static TEST14: &str = include_str!("test14.tig");
pub static TEST15: &str = include_str!("test15.tig");
pub static TEST16: &str = include_str!("test16.tig");
pub static TEST17: &str = include_str!("test17.tig");
pub static TEST18: &str = include_str!("test18.tig");
pub static TEST19: &str = include_str!("test19.tig");

pub static TEST20: &str = include_str!("test20.tig");
pub static TEST21: &str = include_str!("test21.tig");
pub static TEST22: &str = include_str!("test22.tig");
pub static TEST23: &str = include_str!("test23.tig");
pub static TEST24: &str = include_str!("test24.tig");
pub static TEST25: &str = include_str!("test25.tig");
pub static TEST26: &str = include_str!("test26.tig");
pub static TEST27: &str = include_str!("test27.tig");
pub static TEST28: &str = include_str!("test28.tig");
pub static TEST29: &str = include_str!("test29.tig");

pub static TEST30: &str = include_str!("test30.tig");
pub static TEST31: &str = include_str!("test31.tig");
pub static TEST32: &str = include_str!("test32.tig");
pub static TEST33: &str = include_str!("test33.tig");
pub static TEST34: &str = include_str!("test34.tig");
pub static TEST35: &str = include_str!("test35.tig");
pub static TEST36: &str = include_str!("test36.tig");
pub static TEST37: &str = include_str!("test37.tig");
pub static TEST38: &str = include_str!("test38.tig");
pub static TEST39: &str = include_str!("test39.tig");

pub static TEST40: &str = include_str!("test40.tig");
pub static TEST41: &str = include_str!("test41.tig");
pub static TEST42: &str = include_str!("test42.tig");
pub static TEST43: &str = include_str!("test43.tig");
pub static TEST44: &str = include_str!("test44.tig");
pub static TEST45: &str = include_str!("test45.tig");
pub static TEST46: &str = include_str!("test46.tig");
pub static TEST47: &str = include_str!("test47.tig");
pub static TEST48: &str = include_str!("test48.tig");
pub static TEST49: &str = include_str!("test49.tig");

pub static ALL_TESTS: &[(&str, &str)] = &[
    ("merge", MERGE),
    ("queens", QUEENS),
    ("test1", TEST1),
    ("test2", TEST2),
    ("test3", TEST3),
    ("test4", TEST4),
    ("test5", TEST5),
    ("test6", TEST6),
    ("test7", TEST7),
    ("test8", TEST8),
    ("test9", TEST9),

    ("test10", TEST10),
    ("test11", TEST11),
    ("test12", TEST12),
    ("test13", TEST13),
    ("test14", TEST14),
    ("test15", TEST15),
    ("test16", TEST16),
    ("test17", TEST17),
    ("test18", TEST18),
    ("test19", TEST19),

    ("test20", TEST20),
    ("test21", TEST21),
    ("test22", TEST22),
    ("test23", TEST23),
    ("test24", TEST24),
    ("test25", TEST25),
    ("test26", TEST26),
    ("test27", TEST27),
    ("test28", TEST28),
    ("test29", TEST29),

    ("test30", TEST30),
    ("test31", TEST31),
    ("test32", TEST32),
    ("test33", TEST33),
    ("test34", TEST34),
    ("test35", TEST35),
    ("test36", TEST36),
    ("test37", TEST37),
    ("test38", TEST38),
    ("test39", TEST39),

    ("test40", TEST40),
    ("test41", TEST41),
    ("test42", TEST42),
    ("test43", TEST43),
    ("test44", TEST44),
    ("test45", TEST45),
    ("test46", TEST46),
    ("test47", TEST47),
    ("test48", TEST48),
    ("test49", TEST49),
];