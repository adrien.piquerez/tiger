use crate::tiger::syntax::{Exp, VarId, FunId, TypeId, Dec, Type, TypeField, ComparisonOp, Field, LValue};
use crate::tiger::semantic::{TType, FunctionType, TTypeField};
use crate::tiger::Error;
use im::{HashMap, Vector, hashmap};

pub fn analyse(exp: &Exp) -> Result<TType, Error> {
    let environment = Environment::new();
    environment.analyse_exp(exp)
}

#[derive(Clone)]
pub struct Environment {
    variables: HashMap<VarId, TType>,
    functions: HashMap<FunId, FunctionType>,
    types: HashMap<TypeId, TType>
}

impl Environment {
    fn new() -> Self {
        Environment {
            variables: HashMap::new(),
            functions: hashmap! {
                FunId(String::from("getchar")) => FunctionType::new(TType::String, Vector::new())
            },
            types: hashmap! {
                TypeId(String::from("int")) => TType::Int,
                TypeId(String::from("string")) => TType::String
            }
        }
    }

    fn get_variable(&self, id: &VarId) -> Result<TType, Error> {
        let function_type = self.variables.get(id)
            .ok_or(Error::Analysing(format!("Undefined variable {}", id.0)))?;
        Ok(function_type.clone())
    }

    fn get_function(&self, id: &FunId) -> Result<FunctionType, Error> {
        let function_type = self.functions.get(id)
            .ok_or(Error::Analysing(format!("Undefined function {}", id.0)))?;
        Ok(function_type.clone())
    }

    fn get_type(&self, id: &TypeId) -> Result<TType, Error> {
        let ttype = self.types.get(id)
            .ok_or(Error::Analysing(format!("Undefined type {}", id.0)))?;
        Ok(ttype.clone())
    }

    fn declare(mut self, dec: &Dec) -> Result<Environment, Error> {
        match dec {
            Dec::TypeDec(id, type_def) => {
                let ttype = self.create_type(id, type_def)?;
                self.types.insert(id.clone(), ttype);
                Ok(self)
            }

            Dec::VarDec(id, ttype_opt, exp) => {
                let ttype = self.create_variable(ttype_opt.as_ref(), exp)?;
                self.variables.insert(id.clone(), ttype);
                Ok(self)
            }

            Dec::FunDec(id, args, return_type, exp) => {
                let function = self.create_function(args, return_type.as_ref(), exp)?;
                self.functions.insert(id.clone(), function);
                Ok(self)
            }
        }
    }

    fn create_type(&self, id: &TypeId, type_def: &Type) -> Result<TType, Error> {
        match type_def {
            Type::Identifier(id) =>  self.get_type(id),
            Type::Record(fields) => {
                let fields_types = fields.iter().map(
                    |TypeField { id,  type_id }| {
                        let field_type = self.get_type(type_id)?;
                        Ok(TTypeField { id: id.clone(), ttype: field_type })
                    }
                ) .collect::<Result<Vector<_>, _>>()?;
                Ok(TType::Record(id.clone(), fields_types))
            }
            Type::Array(element_type_id) => {
                let element_type = self.get_type(element_type_id)?;
                Ok(TType::Array(id.clone(), Box::new(element_type)))
            }
        }
    }

    fn create_variable(&self, ttype_opt: Option<&TypeId>, exp: &Exp) -> Result<TType, Error> {
        let exp_type = self.analyse_exp(exp)?;
        ttype_opt.map(
            |type_id| {
                let ttype = self.get_type(type_id)?;
                if exp_type == ttype {
                    Ok(())
                } else {
                    Err(Error::Analysing(format!("Expression of type {:?} expected, found {:?}", exp_type, ttype)))
                }
            }
        ).unwrap_or(Ok(())).and(Ok(exp_type))
    }

    fn create_function(&self, args: &Vector<TypeField>, return_type_opt: Option<&TypeId>, exp: &Exp) -> Result<FunctionType, Error> {
        let (scope, args_types) = args.iter().fold(
            Ok((self.clone(), Vector::new())),
            |result, TypeField {id, type_id}| {
                let (mut env, mut args_types) = result?;
                let arg_type = env.get_type(type_id)?;
                env.variables.insert(VarId(id.0.clone()), arg_type.clone());
                args_types.push_back(arg_type);
                Ok((env, args_types))
            }
        )?;
        let exp_type = scope.analyse_exp(exp)?;
        return_type_opt.map(
            |return_type_id| {
                let return_type = self.get_type(return_type_id)?;
                if exp_type == return_type {
                    Ok(())
                } else {
                    Err(Error::Analysing(String::from("")))
                }
            }
        ).unwrap_or(Ok(()))
            .and(Ok(FunctionType::new(exp_type, args_types)))
    }

    pub fn analyse_exp(&self, exp: &Exp) -> Result<TType, Error> {
        match exp {
            Exp::LValue(lvalue) => self.analyse_lvalue(lvalue),

            Exp::Nil => Ok(TType::Nil),

            Exp::Sequencing(exps) => {
                exps.iter().fold(
                    Ok(TType::Unit),
                    |acc, exp| acc.and(self.analyse_exp(exp))
                )
            }

            Exp::NoValue => Ok(TType::Unit),
            Exp::IntegerLiteral(_) => Ok(TType::Int),
            Exp::StringLiteral(_) => Ok(TType::String),

            Exp::Negation(exp) => {
                self.check(exp, &TType::Int).and(Ok(TType::Int))
            }

            Exp::FunctionCall(id, exps) => {
                let FunctionType { arguments, return_type } = self.get_function(id)?;
                exps.iter() // should check for the right number of arguments before zipping
                    .zip(arguments.iter())
                    .fold(
                        Ok(()),
                        |acc, (exp, arg_type)| {
                            acc.and(self.check(exp, arg_type))
                        }
                    ).and(Ok(return_type))
            }

            Exp::Arithmetic(lhs, _, rhs)
            | Exp::Boolean(lhs, _, rhs) => {
                self.check(lhs, &TType::Int)
                    .and(self.check(rhs, &TType::Int))
                    .and(Ok(TType::Int))
            }

            Exp::Comparison(lhs, op, rhs) => {
                self.analyse_exp(lhs).and_then(
                    |lhtype| {
                        self.check(rhs, &lhtype).and(
                            match lhtype {
                                TType::Int | TType::String => Ok(()),
                                TType::Record(_, _) | TType::Array(_, _) => {
                                    match op {
                                        ComparisonOp::Equal | ComparisonOp::NotEqual => Ok(()),
                                        _ => Err(Error::Analysing(format!("invalid operator {:?} for comparing expressions of type {:?}", op, lhtype)))
                                    }
                                }
                                _ => Err(Error::Analysing(format!("cannot compare expressions of type {:?}", lhtype)))
                            }
                        )
                    }
                ).and(Ok(TType::Int))
            }

            Exp::Record(id, fields) => {
                let ttype = self.get_type(id)?;
                match &ttype {
                    TType::Record(_, fields_types) => {
                        fields_types.iter().zip(fields.iter()).map(
                            |(field_type, Field { id, value })| {
                                if id == &field_type.id {
                                    self.check(value, &field_type.ttype)
                                } else {
                                    Err(Error::Analysing(format!("Invalid field of id {:?}, expected {:?}", id, field_type.id)))
                                }
                            }
                        ).fold(
                            Ok(()),
                            |acc, res| acc.and(res)
                        ).and(Ok(ttype))
                    }
                    _ => Err(Error::Analysing(format!("Type {:?} is not a record", id)))
                }
            }

            Exp::Array(id, size, init) => {
                let ttype = self.get_type(id)?;
                match &ttype {
                    TType::Array(_, element_type) => {
                        self.check(size, &TType::Int)
                            .and(self.check(init, element_type.as_ref()))
                            .and(Ok(ttype))
                    }
                    _ => Err(Error::Analysing(format!("Type {:?} is not an array", id)))
                }
            }

            Exp::Assignment(lvalue, exp) => {
                self.analyse_lvalue(lvalue)
                    .and_then(|ttype| self.check(exp, &ttype).and(Ok(ttype)))
            }

            Exp::If(condition, then_exp, else_exp_opt) => {
                self.check(condition, &TType::Int)
                    .and(
                        else_exp_opt.as_ref()
                            .map(
                                |else_exp| {
                                    self.analyse_exp(else_exp.as_ref())
                                }
                            ).unwrap_or(Ok(TType::Nil))
                            .and_then(
                                |else_type| {
                                    self.check(then_exp, &else_type)
                                        .and(Ok(else_type))
                                }
                            )
                    )
            }

            Exp::While(cond, exp) => {
                self.check(cond, &TType::Int)
                    .and(self.check(exp, &TType::Nil))
                    .and(Ok(TType::Nil))
            }

            Exp::For(id, init, end, exp) => {
                let variable = self.get_variable(id)?;
                if variable == TType::Int  {
                    Ok(())?
                }
                else {
                    Err(Error::Analysing(format!("type of variable {:?} should be int, found {:?}", id, variable)))?
                }
                self.check(init, &TType::Int)?;
                self.check(end, &TType::Int)?;
                self.check(exp, &TType::Nil)?;
                Ok(TType::Nil)
            }

            Exp::Break => Ok(TType::Nil),

            Exp::Scope(decs, exps) => {
                let scope: Environment = decs.iter().fold(Ok(self.clone()), |env, dec| env?.declare(dec))?;
                exps.iter().fold(
                    Ok(TType::Unit),
                    |acc, exp| acc.and_then(|_| scope.analyse_exp(exp))
                )
            }
        }
    }

    fn check(&self, exp: &Exp, ttype: &TType) -> Result<(), Error> {
        self.analyse_exp(exp).and_then(
            |exp_type| if &exp_type == ttype {
                Ok(())
            } else {
                Err(Error::Analysing(format!("expected type is {:?}, found {:?}", ttype, exp_type)))
            }
        )
    }

    fn analyse_lvalue(&self, lvalue: &LValue) -> Result<TType, Error> {
        match lvalue {
            LValue::Variable(id) => self.get_variable(id),

            LValue::ArrayElement(array, exp) => {
                let ttype = self.analyse_lvalue(array)?;
                match ttype {
                    TType::Array(_, element_type) => {
                        let exp_ttype = self.analyse_exp(exp)?;
                        match exp_ttype {
                            TType::Int => Ok(*element_type),
                            _ => Err(Error::Analysing(format!("type int expected, found {:?}", exp_ttype)))
                        }
                    }
                    _ => Err(Error::Analysing(format!("array type expected, found {:?}", ttype)))
                }
            }

            LValue::RecordField(record, field_id) => {
                self.analyse_lvalue(record).and_then(
                    |ttype| match ttype {
                        TType::Record(id, fields) => {
                            fields.iter()
                                .find(|field| &field.id == field_id)
                                .map(|field| field.ttype.clone())
                                .ok_or(Error::Analysing(format!("undefined field {:?} in {:?}", field_id, id)))
                        }
                        _ => Err(Error::Analysing(format!("record type expected, found {:?}", ttype)))
                    }
                )
            }
        }
    }
}