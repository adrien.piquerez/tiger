use plex::parser;
use crate::tiger::{Span, Error};
use crate::tiger::lexicon::Token;
use Token::*;
use crate::tiger::syntax::*;
use im::vector::Vector;
use im::vector;

parser! {
    pub fn parser(Token, Span);

    (a, b) {
        Span {
            start: a.start,
            end: b.end,
        }
    }

        expression: Exp {
        open_expression[exp] => exp,
        closed_expression[exp] => exp
    }

    open_expression: Exp {
        If expression[cond] Then expression[exp] =>
            Exp::If(Box::new(cond), Box::new(exp), None),

        If expression[cond] Then closed_expression[exp1] Else open_expression[exp2] =>
            Exp::If(Box::new(cond), Box::new(exp1), Some(Box::new(exp2))),

        Identifier(id) LBracket expression[size] RBracket Of open_expression[value] =>
            Exp::Array(TypeId(id), Box::new(size), Box::new(value)),

        lvalue[value] Assignment open_expression[exp] =>
            Exp::Assignment(value, Box::new(exp)),

        While expression[cond] Do open_expression[exp] =>
            Exp::While(Box::new(cond), Box::new(exp)),

        For var_id[id] Assignment expression[start] To expression[end] Do open_expression[exp] =>
            Exp::For(id, Box::new(start), Box::new(end), Box::new(exp))
    }

    closed_expression: Exp {
        If expression[cond] Then closed_expression[exp1] Else closed_expression[exp2] =>
            Exp::If(Box::new(cond), Box::new(exp1), Some(Box::new(exp2))),

        Identifier(id) LBracket expression[size] RBracket Of closed_expression[value] =>
            Exp::Array(TypeId(id), Box::new(size), Box::new(value)),

        lvalue[value] Assignment closed_expression[exp] =>
            Exp::Assignment(value, Box::new(exp)),

        While expression[cond] Do closed_expression[exp] =>
            Exp::While(Box::new(cond), Box::new(exp)),

        For var_id[id] Assignment expression[start] To expression[end] Do closed_expression[exp] =>
            Exp::For(id, Box::new(start), Box::new(end), Box::new(exp)),

        operation_expression[exp] => exp
    }

    operation_expression: Exp {
        comparison[lhs] boolean_op[op] operation_expression[rhs] =>
            Exp::Boolean(Box::new(lhs), op, Box::new(rhs)),

        comparison[exp] => exp
    }

    comparison: Exp {
        arithmetic[lhs] comparison_op[op] arithmetic[rhs] =>
            Exp::Comparison(Box::new(lhs), op, Box::new(rhs)),

        arithmetic[exp] => exp
    }

    arithmetic: Exp {
        arithmetic[lhs] Times sum_or_sub[rhs] =>
            Exp::Arithmetic(Box::new(lhs), ArithmeticOp::Times, Box::new(rhs)),

        arithmetic[lhs] Div sum_or_sub[rhs] =>
            Exp::Arithmetic(Box::new(lhs), ArithmeticOp::Div, Box::new(rhs)),

        sum_or_sub[exp] => exp
    }

    sum_or_sub: Exp {
        sum_or_sub[lhs] Plus simple_expression[rhs] =>
            Exp::Arithmetic(Box::new(lhs), ArithmeticOp::Plus, Box::new(rhs)),

        sum_or_sub[lhs] Minus simple_expression[rhs] =>
            Exp::Arithmetic(Box::new(lhs), ArithmeticOp::Minus, Box::new(rhs)),

        simple_expression[exp] => exp
    }

    simple_expression: Exp {
        lvalue[value] => Exp::LValue(value),
        Nil => Exp::Nil,
        LParen expseq[exps] RParen => match exps.len() {
            0 => Exp::NoValue,
            1 => exps.head().cloned().unwrap(),
            _ => Exp::Sequencing(exps)
        },

        IntegerLiteral(n) => Exp::IntegerLiteral(n),
        Minus simple_expression[exp] => Exp::Negation(Box::new(exp)),
        StringLiteral(s) => Exp::StringLiteral(s),
        fun_id[id] LParen arguments[args] RParen => Exp::FunctionCall(id, args),
        type_id[id] LBrace fields[fields] RBrace => Exp::Record(id, fields),
        Break => Exp::Break,
        Let declarations[decs] In expseq[exps] End => Exp::Scope(decs, exps)
    }

    lvalue: LValue {
        Identifier(id) lvalue_selection[selection] => selection.of(LValue::Variable(VarId(id)))
    }

    lvalue_selection: LValueSelection {
        Dot field_id[field] lvalue_selection[selection] => selection.of_field(field),
        LBracket expression[exp] RBracket lvalue_selection[selection] => selection.of_element(exp),
        => LValueSelection::None
    }

    expseq: Vector<Exp> {
        => vector!(),
        non_empty_expseq[exps] => exps,
    }

    non_empty_expseq: Vector<Exp> {
        non_empty_expseq[mut exps] SemiColon expression[exp] => {exps.push_back(exp); exps},
        expression[exp] => vector!(exp)
    }

    arguments: Vector<Exp> {
        non_empty_args_list[args] => args,
        => vector!(),
    }

    non_empty_args_list: Vector<Exp> {
        non_empty_args_list[mut args] Comma expression[arg] => {args.push_back(arg); args},
        expression[arg] => vector!(arg)
    }

    fields: Vector<Field> {
        non_empty_fields[fields] => fields,
        => vector!(),
    }

    non_empty_fields: Vector<Field> {
        non_empty_fields[mut fields] Comma field[field] => {fields.push_back(field); fields},
        field[field] => vector!(field)
    }

    field: Field {
        field_id[id] Equal expression[exp] => Field::new(id, exp)
    }

    comparison_op: ComparisonOp {
        Equal => ComparisonOp::Equal,
        NotEqual => ComparisonOp::NotEqual,
        SupOrEqual => ComparisonOp::SupOrEqual,
        InfOrEqual => ComparisonOp::InfOrEqual,
        Sup => ComparisonOp::Sup,
        Inf => ComparisonOp::Inf
    }

    boolean_op: BooleanOp {
        And => BooleanOp::And,
        Or => BooleanOp::Or
    }

    declarations: Vector<Dec> {
        => vector!(),
        non_empty_declarations[decs] => decs
    }

    non_empty_declarations: Vector<Dec> {
        non_empty_declarations[mut decs] declaration[dec] => { decs.push_back(dec); decs }
        declaration[dec] => vector!(dec)
    }

    declaration: Dec {
        type_declaration[type_dec] => type_dec,
        var_declaration[var_dec] => var_dec,
        fun_declaration[fun_dec] => fun_dec
    }

    type_declaration: Dec {
        TypeDef type_id[id] Equal ty[ty] => Dec::TypeDec(id, ty),
    }

    ty: Type {
        type_id[id] => Type::Identifier(id),
        LBrace type_fields[fields] RBrace => Type::Record(fields),
        ArrayDef Identifier(id) => Type::Array(TypeId(id))
    }

    type_id: TypeId {
        Identifier(id) => TypeId(id)
    }

    type_fields: Vector<TypeField> {
        => vector!(),
        not_empty_type_fields[fields] => fields
    }

    not_empty_type_fields: Vector<TypeField> {
        not_empty_type_fields[mut fields] Comma type_field[field] =>
            { fields.push_back(field); fields },

        type_field[field] => vector!(field)
    }

    type_field: TypeField {
        field_id[field_id] Colon type_id[type_id] => TypeField::new(field_id, type_id)
    }

    field_id: FieldId {
        Identifier(id) => FieldId(id)
    }

    var_declaration: Dec {
        VarDef var_id[id] Assignment expression[exp] => Dec::VarDec(id, None, exp),

        VarDef var_id[id] Colon type_id[ty] Assignment expression[exp] =>
            Dec::VarDec(id, Some(ty), exp)
    }

    var_id: VarId {
        Identifier(id) => VarId(id)
    }

    fun_declaration: Dec {
        FunctionDef fun_id[id] LParen type_fields[fields] RParen Equal expression[exp] =>
            Dec::FunDec(id, fields, None, exp),

        FunctionDef fun_id[id] LParen type_fields[fields] RParen Colon type_id[ty] Equal expression[exp] =>
            Dec::FunDec(id, fields, Some(ty), exp)
    }

    fun_id: FunId {
        Identifier(id) => FunId(id)
    }
}

pub fn parse<L>(lexer: L) -> Result<Exp, Error> where L: Iterator<Item = Result<(Token, Span), Error>> {
    let tokens = lexer.collect::<Result<Vector<_>, _>>()?;
    let result: Result<Exp, (Option<(Token, Span)>, &str)> = parser(tokens.into_iter());
    result.map_err(|err| Error::Parsing(err.0, String::from(err.1)))
}

enum LValueSelection {
    Field(Box<LValueSelection>, FieldId),
    Element(Box<LValueSelection>, Exp),
    None
}

impl LValueSelection {

    fn of_field(self, field: FieldId) -> LValueSelection {
        match self {
            LValueSelection::None =>
                LValueSelection::Field(Box::new(LValueSelection::None), field),

            LValueSelection::Field(selection, other_field) =>
                LValueSelection::Field(Box::new(selection.of_field(field)), other_field),

            LValueSelection::Element(selection, exp) =>
                LValueSelection::Element(Box::new(selection.of_field(field)), exp)
        }
    }

    fn of_element(self, exp: Exp) -> LValueSelection {
        match self {
            LValueSelection::None =>
                LValueSelection::Element(Box::new(LValueSelection::None), exp),

            LValueSelection::Field(selection, field) =>
                LValueSelection::Field(Box::new(selection.of_element(exp)), field),

            LValueSelection::Element(selection, other_exp) =>
                LValueSelection::Element(Box::new(selection.of_element(exp)), other_exp)
        }
    }

    fn of(self, lvalue: LValue) -> LValue {
        match self {
            LValueSelection::None => lvalue,

            LValueSelection::Field(selection, field) =>
                LValue::RecordField(Box::new(selection.of(lvalue)), field),

            LValueSelection::Element(selection, exp) =>
                LValue::ArrayElement(Box::new(selection.of(lvalue)), Box::new(exp))
        }
    }
}

#[test]
fn test_type_dec() {
    let span = Span {
        start: 0,
        end: 0,
    };
    let source = vector!(
        Let, TypeDef, Identifier("a".to_owned()), Equal, Identifier("b".to_owned()),
        In, LParen, RParen, End
    ).into_iter().map(|token| (token, span));
    let result = parser(source);
    let expected = Exp::Scope(
        vector!(
            Dec::TypeDec(
                TypeId("a".to_owned()),
                Type::Identifier(TypeId("b".to_owned()))
            )
        ),
        vector!(Exp::NoValue)
    );
    assert_eq!(result, Ok(expected));
}

#[test]
fn test_record_type_dec() {
    let span = Span { start: 0, end: 0 };
    let source = vector!(
        Let,
        TypeDef, Identifier("a".to_owned()), Equal,
        LBrace,
        Identifier("b".to_owned()), Colon, Identifier("c".to_owned()), Comma,
        Identifier("d".to_owned()), Colon, Identifier("e".to_owned()),
        RBrace,
        In, LParen, RParen, End
    ).into_iter().map(|tok| (tok, span));
    let result = parser(source);
    let expectation = Exp::Scope(
        vector!(
            Dec::TypeDec(
                TypeId("a".to_owned()),
                Type::Record(
                    vector!(
                        TypeField::new(FieldId("b".to_owned()), TypeId("c".to_owned())),
                        TypeField::new(FieldId("d".to_owned()), TypeId("e".to_owned()))
                    )
                )
            )
        ),
        vector!(Exp::NoValue)
    );
    assert_eq!(result, Ok(expectation));
}

#[test]
fn test_array_type_dec() {
    let span = Span { start: 0, end: 0 };
    let source = vector!(
        Let,
        TypeDef, Identifier("a".to_owned()), Equal,
        ArrayDef, Identifier("b".to_owned()),
        In, LParen, RParen, End
    ).into_iter().map(|tok| (tok, span));
    let result = parser(source);
    let expectation = Exp::Scope(
        vector!(
            Dec::TypeDec(
                TypeId("a".to_owned()),
                Type::Array(TypeId("b".to_owned()))
            )
        ),
        vector!(Exp::NoValue)
    );
    assert_eq!(result, Ok(expectation));
}

#[test]
fn test_identifier() {
    let span = Span { start: 0, end: 0 };
    let source = vector!(
        Identifier("b".to_owned())
    ).into_iter().map(|tok| (tok, span));
    let result = parser(source);
    let expectation = Exp::LValue(LValue::Variable(VarId("b".to_owned())));
    assert_eq!(result, Ok(expectation));
}

#[test]
fn test_record_field() {
    let span = Span { start: 0, end: 0 };
    let source = vector!(
        Identifier("b".to_owned()), Dot, Identifier("c".to_owned())
    ).into_iter().map(|tok| (tok, span));
    let result = parser(source);
    let expectation = Exp::LValue(
        LValue::RecordField(
            Box::new(LValue::Variable(VarId("b".to_owned()))),
            FieldId("c".to_owned())
        )
    );
    assert_eq!(result, Ok(expectation));
}

#[test]
fn test_array_element() {
    let span = Span { start: 0, end: 0 };
    let source = vector!(
        Identifier("b".to_owned()), LBracket, Identifier("c".to_owned()), RBracket
    ).into_iter().map(|tok| (tok, span));
    let result = parser(source);
    let expectation = Exp::LValue(
        LValue::ArrayElement(
            Box::new(LValue::Variable(VarId("b".to_owned()))),
            Box::new(Exp::LValue(LValue::Variable(VarId("c".to_owned()))))
        )
    );
    assert_eq!(result, Ok(expectation));
}

#[test]
fn test_record_field_element() {
    let span = Span { start: 0, end: 0 };
    let source = vector!(
        Identifier("b".to_owned()),
        Dot, Identifier("c".to_owned()),
        LBracket, Identifier("d".to_owned()), RBracket
    ).into_iter().map(|tok| (tok, span));
    let result = parser(source);
    let expectation = Exp::LValue(
        LValue::ArrayElement(
            Box::new(LValue::RecordField(
                Box::new(LValue::Variable(VarId("b".to_owned()))),
                FieldId("c".to_owned())
            )),
            Box::new(Exp::LValue(LValue::Variable(VarId("d".to_owned()))))
        )
    );
    assert_eq!(result, Ok(expectation));
}

#[test]
fn test_array_element_field_var_dec() {
    let span = Span { start: 0, end: 0 };
    let source = vector!(
        Identifier("b".to_owned()),
        LBracket, Identifier("c".to_owned()), RBracket,
        Dot, Identifier("d".to_owned())
    ).into_iter().map(|tok| (tok, span));
    let result = parser(source);
    let expectation = Exp::LValue(
        LValue::RecordField(
            Box::new(LValue::ArrayElement(
                Box::new(LValue::Variable(VarId("b".to_owned()))),
                Box::new(Exp::LValue(LValue::Variable(VarId("c".to_owned()))))
            )),
            FieldId("d".to_owned())
        )
    );
    assert_eq!(result, Ok(expectation));
}

#[test]
fn test_nil_var_dec_with_type() {
    let span = Span { start: 0, end: 0 };
    let source = vector!(
        Let, VarDef, Identifier("a".to_owned()), Colon, Identifier("b".to_owned()), Assignment, Nil,
        In, LParen, RParen, End
    ).into_iter().map(|tok| (tok, span));
    let result = parser(source);
    let expectation = Exp::Scope(
        vector!(
            Dec::VarDec(VarId("a".to_owned()), Some(TypeId("b".to_owned())), Exp::Nil)
        ),
        vector!(Exp::NoValue)
    );
    assert_eq!(result, Ok(expectation));
}

#[test]
fn test_sequenced_var_dec() {
    let span = Span { start: 0, end: 0 };
    let source = vector!(
        LParen,
        Identifier("b".to_owned()), SemiColon,
        Identifier("c".to_owned()),
        RParen
    ).into_iter().map(|tok| (tok, span));
    let result = parser(source);
    let expectation = Exp::Sequencing(
        vector!(
            Exp::LValue(LValue::Variable(VarId("b".to_owned()))),
            Exp::LValue(LValue::Variable(VarId("c".to_owned())))
        )
    );
    assert_eq!(result, Ok(expectation));
}

#[test]
fn test_negative_integer_literal() {
    let span = Span { start: 0, end: 0 };
    let source = vector!(
        Minus, IntegerLiteral(1)
    ).into_iter().map(|tok| (tok, span));
    let result = parser(source);
    let expectation = Exp::Negation(Box::new(Exp::IntegerLiteral(1)));
    assert_eq!(result, Ok(expectation));
}

#[test]
fn test_function_call() {
    let span = Span { start: 0, end : 0 };
    let source = vector!(
        Identifier("b".to_owned()), LParen, RParen
    ).into_iter().map(|tok| (tok, span));
    let result = parser(source);
    let expectation = Exp::FunctionCall(FunId("b".to_owned()), vector!());
    assert_eq!(result, Ok(expectation));
}

#[test]
fn test_addition() {
    let span = Span { start: 0, end: 0 };
    let source = vector!(
        IntegerLiteral(123), Plus, IntegerLiteral(456)
    ).into_iter().map(|tok| (tok, span));
    let result = parser(source);
    let expectation = Exp::Arithmetic(
        Box::new(Exp::IntegerLiteral(123)),
        ArithmeticOp::Plus,
        Box::new(Exp::IntegerLiteral(456))
    );
    assert_eq!(result, Ok(expectation));
}

#[test]
fn test_multiplication() {
    let span = Span { start: 0, end: 0 };
    let source = vector!(
        IntegerLiteral(123), Times,
        IntegerLiteral(456), Minus, IntegerLiteral(789)
    ).into_iter().map(|tok| (tok, span));
    let result = parser(source);
    let expectation = Exp::Arithmetic(
        Box::new(Exp::IntegerLiteral(123)),
        ArithmeticOp::Times,
        Box::new(
            Exp::Arithmetic(
                Box::new(Exp::IntegerLiteral(456)),
                ArithmeticOp::Minus,
                Box::new(Exp::IntegerLiteral(789))
            )
        )
    );
    assert_eq!(result, Ok(expectation));
}

#[test]
fn test_comparison() {
    let span = Span { start: 0, end: 0 };
    let source = vector!(
        Identifier("b".to_owned()), Inf, IntegerLiteral(456)
    ).into_iter().map(|tok| (tok, span));
    let result = parser(source);
    let expectation = Exp::Comparison(
        Box::new(Exp::LValue(LValue::Variable(VarId("b".to_owned())))),
        ComparisonOp::Inf,
        Box::new(Exp::IntegerLiteral(456))
    );
    assert_eq!(result, Ok(expectation));
}

#[test]
fn test_arithmetic_comparison() {
    let span = Span { start: 0, end: 0 };
    let source = vector!(
        IntegerLiteral(123), Times,
        IntegerLiteral(456), SupOrEqual, IntegerLiteral(789)
    ).into_iter().map(|tok| (tok, span));
    let result = parser(source);
    let expectation = Exp::Comparison(
        Box::new(
            Exp::Arithmetic(
                Box::new(Exp::IntegerLiteral(123)),
                ArithmeticOp::Times,
                Box::new(Exp::IntegerLiteral(456))
            )
        ),
        ComparisonOp::SupOrEqual,
        Box::new(Exp::IntegerLiteral(789))
    );
    assert_eq!(result, Ok(expectation));
}

#[test]
fn test_boolean_operation() {
    let span = Span { start: 0, end: 0 };
    let source = vector!(
        IntegerLiteral(0), And, IntegerLiteral(1)
    ).into_iter().map(|tok| (tok, span));
    let result = parser(source);
    let expectation = Exp::Boolean(
        Box::new(Exp::IntegerLiteral(0)),
        BooleanOp::And,
        Box::new(Exp::IntegerLiteral(1))
    );
    assert_eq!(result, Ok(expectation));
}

#[test]
fn test_comparison_boolean() {
    let span = Span { start: 0, end: 0 };
    let source = vector!(
        Identifier("b".to_owned()), Or,
        IntegerLiteral(456), SupOrEqual, IntegerLiteral(789)
    ).into_iter().map(|tok| (tok, span));
    let result = parser(source);
    let expectation = Exp::Boolean(
        Box::new(Exp::LValue(LValue::Variable(VarId("b".to_owned())))),
        BooleanOp::Or,
        Box::new(
            Exp::Comparison(
                Box::new(Exp::IntegerLiteral(456)),
                ComparisonOp::SupOrEqual,
                Box::new(Exp::IntegerLiteral(789))
            )
        )
    );
    assert_eq!(result, Ok(expectation));
}

#[test]
fn test_record() {
    let span = Span { start: 0, end : 0 };
    let source = vector!(
        Identifier("b".to_owned()), LBrace,
        Identifier("c".to_owned()), Equal, IntegerLiteral(123),
        RBrace
    ).into_iter().map(|tok| (tok, span));
    let result = parser(source);
    let expectation = Exp::Record(
        TypeId("b".to_owned()),
        vector!(Field::new(FieldId("c".to_owned()), Exp::IntegerLiteral(123)))
    );
    assert_eq!(result, Ok(expectation));
}

#[test]
fn test_array() {
    let span = Span { start: 0, end: 0 };
    let source = vector!(
        Identifier("b".to_owned()), LBracket, IntegerLiteral(123), RBracket,
        Of, StringLiteral("hello".to_owned())
    ).into_iter().map(|tok| (tok, span));
    let result = parser(source);
    let expectation = Exp::Array(
        TypeId("b".to_owned()),
        Box::new(Exp::IntegerLiteral(123)),
        Box::new(Exp::StringLiteral("hello".to_owned()))
    );
    assert_eq!(result, Ok(expectation));
}

#[test]
fn test_assignment() {
    let span = Span { start: 0, end: 0 };
    let source = vector!(
        Identifier("b".to_owned()), Assignment, IntegerLiteral(123)
    ).into_iter().map(|tok| (tok, span));
    let result = parser(source);
    let expectation = Exp::Assignment(
        LValue::Variable(VarId("b".to_owned())),
        Box::new(Exp::IntegerLiteral(123))
    );
    assert_eq!(result, Ok(expectation));
}

#[test]
fn test_if_then_else_if_then_else() {
    let span = Span { start: 0, end: 0 };
    let source = vector!(
        If, Identifier("b".to_owned()), Then, IntegerLiteral(123),
        Else, If, Identifier("c".to_owned()), Then, IntegerLiteral(234),
        Else, IntegerLiteral(345)
    ).into_iter().map(|tok| (tok, span));
    let result = parser(source);
    let expectation = Exp::If(
        Box::new(Exp::LValue(LValue::Variable(VarId("b".to_owned())))),
        Box::new(Exp::IntegerLiteral(123)),
        Some(
            Box::new(
                Exp::If(
                    Box::new(Exp::LValue(LValue::Variable(VarId("c".to_owned())))),
                    Box::new(Exp::IntegerLiteral(234)),
                    Some(Box::new(Exp::IntegerLiteral(345)))
                )
            )
        )
    );
    assert_eq!(result, Ok(expectation));
}

#[test]
fn test_function_dec_without_type() {
    let span = Span { start: 0, end: 0 };
    let source = vector!(
        Let,
        FunctionDef, Identifier("a".to_owned()), LParen, RParen,
        Equal, StringLiteral("hello".to_owned()),
        In, LParen, RParen, End
    ).into_iter().map(|tok| (tok, span));
    let result = parser(source);
    let expectation = Exp::Scope(
        vector!(
            Dec::FunDec(
                FunId("a".to_owned()), vector!(), None,
                Exp::StringLiteral("hello".to_owned())
            )
        ),
        vector!(Exp::NoValue)
    );
    assert_eq!(result, Ok(expectation));
}
