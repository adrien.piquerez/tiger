use crate::tiger::lexicon::Token;
use crate::tiger::Span;
use crate::tiger::Error;
use logos::Logos;

#[derive(Logos)]
enum LexerToken {
    #[error] Error,
    #[end] EOF,

    #[regex = r"\s+"] Whitespace,
    #[regex = r"/\*[^\*/]*\*/"] Comment,

    #[token = "{"] LBrace,
    #[token = "}"] RBrace,
    #[token = "["] LBracket,
    #[token = "]"] RBracket,
    #[token = "("] LParen,
    #[token = ")"] RParen,

    #[token = ":="] Assignment,

    #[token = ","] Comma,
    #[token = ":"] Colon,
    #[token = "."] Dot,
    #[token = ";"] SemiColon,

    #[token = "-"] Minus,
    #[token = "+"] Plus,
    #[token = "*"] Times,
    #[token = "/"] Div,

    #[token = "&"] And,
    #[token = "|"] Or,

    #[token = "="] Equal,
    #[token = "<>"] NotEqual,
    #[token = ">="] SupOrEqual,
    #[token = "<="] InfOrEqual,
    #[token = ">"] Sup,
    #[token = "<"] Inf,

    #[token = "nil"] Nil,

    #[token = "type"] TypeDef,
    #[token = "var"] VarDef,
    #[token = "function"] FunctionDef,

    #[token = "array of"] ArrayDef,
    #[token= "of"] Of,

    #[token = "if"] If,
    #[token = "then"] Then,
    #[token = "else"] Else,

    #[token = "while"] While,
    #[token = "for"] For,
    #[token = "to"] To,
    #[token = "do"] Do,
    #[token = "break"] Break,

    #[token = "let"] Let,
    #[token = "in"] In,
    #[token = "end"] End,

    #[regex = r"[0-9]*"] Integer,
    #[regex = r#""[^"]*""#] String,
    #[regex = r"[a-zA-Z_][a-zA-Z_1-9]*"] Identifier
}

pub struct Lexer<'a> {
    lexer: logos::Lexer<LexerToken, &'a str>
}

impl<'a> Lexer<'a> {
    pub fn new(input: &str) -> Lexer {
        Lexer { lexer: LexerToken::lexer(input) }
    }
}

impl<'a> Iterator for Lexer<'a> {
    type Item = Result<(Token, Span), Error>;
    fn next(&mut self) -> Option<Result<(Token, Span), Error>> {
        loop {
            let token = match self.lexer.token {
                LexerToken::Error => {
                    let span = Span::from(self.lexer.range());
                    return Some(Err(Error::Lexing(span, String::from("Lexing failed"))))
                },
                LexerToken::EOF => return None,
                LexerToken::Whitespace | LexerToken::Comment => {
                    self.lexer.advance();
                    continue
                }

                LexerToken::LBrace => Token::LBrace,
                LexerToken::RBrace => Token::RBrace,
                LexerToken::LBracket => Token::LBracket,
                LexerToken::RBracket => Token::RBracket,
                LexerToken::LParen => Token::LParen,
                LexerToken::RParen => Token::RParen,

                LexerToken::Assignment => Token::Assignment,

                LexerToken::Comma => Token::Comma,
                LexerToken::Colon => Token::Colon,
                LexerToken::Dot => Token::Dot,
                LexerToken::SemiColon => Token::SemiColon,

                LexerToken::Minus => Token::Minus,
                LexerToken::Plus => Token::Plus,
                LexerToken::Times => Token::Times,
                LexerToken::Div => Token::Div,

                LexerToken::And => Token::And,
                LexerToken::Or => Token::Or,

                LexerToken::Equal => Token::Equal,
                LexerToken::NotEqual => Token::NotEqual,
                LexerToken::SupOrEqual => Token::SupOrEqual,
                LexerToken::InfOrEqual => Token::InfOrEqual,
                LexerToken::Sup => Token::Sup,
                LexerToken::Inf => Token::Inf,

                LexerToken::Nil => Token::Nil,

                LexerToken::TypeDef => Token::TypeDef,
                LexerToken::VarDef => Token::VarDef,
                LexerToken::FunctionDef => Token::FunctionDef,

                LexerToken::ArrayDef => Token::ArrayDef,
                LexerToken::Of => Token::Of,

                LexerToken::If => Token::If,
                LexerToken::Then => Token::Then,
                LexerToken::Else => Token::Else,

                LexerToken::While => Token::While,
                LexerToken::For => Token::For,
                LexerToken::To => Token::To,
                LexerToken::Do => Token::Do,
                LexerToken::Break => Token::Break,

                LexerToken::Let => Token::Let,
                LexerToken::In => Token::In,
                LexerToken::End => Token::End,

                LexerToken::Integer => {
                    let slice = self.lexer.slice();
                    Token::IntegerLiteral(slice.parse().unwrap())
                }
                LexerToken::String => {
                    let slice = self.lexer.slice();
                    Token::StringLiteral(String::from(&slice[1 .. slice.len()-1]))
                }
                LexerToken::Identifier => {
                    let slice = self.lexer.slice();
                    Token::Identifier(String::from(slice))
                }
            };
            let span = Span::from(self.lexer.range());
            self.lexer.advance();
            return Some(Ok((token, span)));
        }
    }
}