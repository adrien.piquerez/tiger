pub mod tests;
pub mod lexicon;
pub mod lexer;
pub mod syntax;
pub mod parser;
pub mod analyser;
pub mod semantic;

use lexicon::Token;
use std::ops::Range;

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Span {
    pub start: usize,
    pub end: usize
}

impl From<Range<usize>> for Span {
    fn from(range: Range<usize>) -> Self {
        Span { start: range.start, end: range.end }
    }
}

#[derive(Debug)]
pub enum Error {
    Lexing(Span, String),
    Parsing(Option<(Token, Span)>, String),
    Analysing(String)
}