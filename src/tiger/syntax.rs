use im::vector::Vector;

#[derive(Debug, PartialEq, Clone)]
pub enum Dec {
    TypeDec(TypeId, Type),
    VarDec(VarId, Option<TypeId>, Exp),
    FunDec(FunId, Vector<TypeField>, Option<TypeId>, Exp)
}

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
pub struct TypeId(pub String);

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
pub struct VarId(pub String);

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
pub struct FunId(pub String);

#[derive(Debug, PartialEq, Clone)]
pub enum Type {
    Identifier(TypeId),
    Record(Vector<TypeField>),
    Array(TypeId)
}

#[derive(Debug, PartialEq, Clone)]
pub struct TypeField {
    pub id: FieldId,
    pub type_id: TypeId
}

impl TypeField {
    pub fn new(id: FieldId, type_id: TypeId) -> TypeField {
        TypeField { id: id, type_id: type_id }
    }
}

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
pub struct FieldId(pub String);

#[derive(Debug, PartialEq, Clone)]
pub enum Exp {
    LValue(LValue),
    Nil,
    Sequencing(Vector<Exp>),
    NoValue,
    IntegerLiteral(i32),
    StringLiteral(String),
    Negation(Box<Exp>),
    FunctionCall(FunId, Vector<Exp>),
    Arithmetic(Box<Exp>, ArithmeticOp, Box<Exp>),
    Comparison(Box<Exp>, ComparisonOp, Box<Exp>),
    Boolean(Box<Exp>, BooleanOp, Box<Exp>),
    Record(TypeId, Vector<Field>),
    Array(TypeId, Box<Exp>, Box<Exp>),
    Assignment(LValue, Box<Exp>),
    If(Box<Exp>, Box<Exp>, Option<Box<Exp>>),
    While(Box<Exp>, Box<Exp>),
    For(VarId, Box<Exp>, Box<Exp>, Box<Exp>),
    Break,
    Scope(Vector<Dec>, Vector<Exp>)
}

#[derive(Debug, PartialEq, Clone)]
pub enum LValue {
    Variable(VarId),
    RecordField(Box<LValue>, FieldId),
    ArrayElement(Box<LValue>, Box<Exp>)
}

#[derive(Debug, PartialEq, Clone)]
pub enum ArithmeticOp {
    Plus, Minus, Times, Div
}

#[derive(Debug, PartialEq, Clone)]
pub enum ComparisonOp {
    Equal, NotEqual, SupOrEqual, InfOrEqual, Sup, Inf,
}

#[derive(Debug, PartialEq, Clone)]
pub enum BooleanOp {
    And, Or
}

#[derive(Debug, PartialEq, Clone)]
pub struct Field { pub id: FieldId, pub value: Exp }

impl Field {
    pub fn new(id: FieldId, value: Exp) -> Field {
        Field {
            id: id,
            value: value
        }
    }
}