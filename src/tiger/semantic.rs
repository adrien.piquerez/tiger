use im::vector::Vector;
use crate::tiger::syntax::{TypeId, FieldId};

#[derive(PartialEq, Clone, Debug)]
pub enum TType {
    Int,
    String,
    Record(TypeId, Vector<TTypeField>),
    Array(TypeId, Box<TType>),
    Nil,
    Unit
}

#[derive(PartialEq, Clone, Debug)]
pub struct TTypeField {
    pub id: FieldId,
    pub ttype: TType
}

#[derive(Clone)]
pub struct FunctionType {
    pub return_type: TType,
    pub arguments: Vector<TType>
}

impl FunctionType {
    pub fn new(return_type: TType, arguments: Vector<TType>) -> FunctionType {
        FunctionType { return_type, arguments }
    }
}