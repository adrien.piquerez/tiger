#[derive(Debug, PartialEq, Clone)]
pub enum Token {
    LBrace, RBrace, LBracket, RBracket, LParen, RParen,
    Assignment,
    Comma, Colon, Dot, SemiColon,
    Minus, Plus, Times, Div,
    And, Or,
    Equal, NotEqual, SupOrEqual, InfOrEqual, Sup, Inf,
    Nil,
    TypeDef, VarDef, FunctionDef,
    ArrayDef, Of,
    If, Then, Else,
    While, For, To, Do, Break,
    Let, In, End,
    IntegerLiteral(i32),
    StringLiteral(String),
    Identifier(String),
}