mod tiger;
mod chap1;
mod chap2;
mod chap3;
mod chap5;

fn main() {
  println!("Chap 1:");
  chap1::program::execute();
  println!();
  println!("Chap 2:");
  chap2::program::execute();
  println!();
  println!("Chap 3:");
  chap3::program::execute();
  println!();
  println!("Chap 5:");
  chap5::program::execute();

}
