use std::time::Instant;
use crate::tiger::tests;
use crate::tiger::lexer::Lexer;
use crate::tiger::parser;

pub fn execute() {
    for (name, test) in tests::ALL_TESTS {
        let start = Instant::now();
        let lexer = Lexer::new(test);
        let result = parser::parse(lexer);
        match result {
            Err(e) => println!("Error while parsing {}: {:?}", name, e),
            Ok(_exp) => println!("parsed {} in {:?}", name, start.elapsed())
        }
    }
}