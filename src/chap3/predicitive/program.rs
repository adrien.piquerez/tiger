use std::collections::VecDeque;

enum Token {
    If, Then, Else, Begin, End, Print, Semi, Num, Eq
}

trait Parser {
    fn token(&self) -> &Token;
    fn eat(&mut self, tok: Token) -> ();
    fn error(&self) -> () { panic!("invalid input") }

    fn parse(&mut self) -> () {
        self.prod_s()
    }

    fn prod_s(&mut self) -> () {
        match self.token() {
            Token::If => {
                self.eat(Token::If);
                self.prod_e();
                self.eat(Token::Then);
                self.prod_s();
                self.eat(Token::Else);
                self.prod_s();
            }
            Token::Begin => {
                self.eat(Token::Begin);
                self.prod_s();
                self.prod_l();
            }
            Token::Print => {
                self.eat(Token::Print);
                self.prod_e();
            }
            _ => self.error()
        }
    }

    fn prod_e(&mut self) -> () {
        match self.token() {
            Token::Num => {
                self.eat(Token::Num);
                self.eat(Token::Eq);
                self.eat(Token::Num)
            }
            _ => self.error()
        }
    }

    fn prod_l(&mut self) -> () {
        match self.token() {
            Token::End => self.eat(Token::End),
            Token::Semi => {
                self.eat(Token::Semi); self.prod_s(); self.prod_l()
            }
            _ => self.error()
        }
    }
}

impl Parser for VecDeque<Token> {
    fn token(&self) -> &Token {
        match self.front() {
            Some(token) => token,
            None => panic!("no token found")
        }
    }

    fn eat(&mut self, _tok: Token) -> () {
        match self.pop_front() {
            Some(_tok) => {}
            None => self.error()
        }
    }
}

pub fn execute() -> () {
    VecDeque::from(vec![
        Token::If, Token::Num, Token::Eq, Token::Num,
        Token::Then, Token::Print, Token::Num, Token::Eq, Token::Num,
        Token::Else, Token::Begin, Token::Print, Token::Num, Token::Eq, Token::Num,
        Token::Semi, Token::Print, Token::Num, Token::Eq, Token::Num,
        Token::End]).parse();
    println!("ok")
}