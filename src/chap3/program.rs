use crate::chap3::predicitive;
use crate::chap3::plex;

pub fn execute() {
    println!("Predictive LL(1) Parser:");
    predicitive::program::execute();
    println!();
    println!("Plex Parser:");
    plex::program::execute();
    println!();
}