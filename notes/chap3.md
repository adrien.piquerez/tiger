#Parsing

The additional expressive power gained by recursion is just  what we need for parsing.
Also, once we have abbreviations (a symbol stands for some regular expression) with recursion,
we do not need alternation except at the top level of expressions, because the defintion
````
expr = ab(c|d)e
````
can always be rewritten using an auxiliary definition
````
aux = c | d
expr = ab aux e
````
The Kleene closure is not necessary, since we can rewrite it so that
````
expr = (abc)*
````
becomes
````
expr = (abc)expr
expr = €
````
What we have left is a very simple notation, called _context-free grammars_.
Just as regular expressions can be used to define lexical strucutre in a static, declarative way,
grammars define syntactic structure declaratively. But we will need something more powerful
than finite automata to parse languages described by grammars.

## Context Free Grammars

### Ambiguous Grammars

A grammar is ambiguous if it can derive a sentence with two different parse tree. 
````
E -> E + E
E -> num
````
Fortunately, we an often transform ambiguous grammars to unambiguous grammars.
````
E -> E + T
T -> num
````

## Predictive Parsing

Recursive-descent, or _predictive_, parsing work only in grammars wher the _first terminal symbol_
of each  subexpression provides enough information to choose wich production to use. 

### Contructing a Predictive Parser

Grammars whose predictive parsing tables contain no duplicate entries are called LL(1)
(_Left-to-right parse_, _Leftmost-derivation_, _1-symbol lookahead_)

### Eliminating Left Recursion

````
E -> E + T
E -> T
````

````
E -> TE'
E' -> + TE' 
E' ->
````

### Left Factoring
````
S -> if E then S else S
S -> if E then S
````

````
S -> if E then S X
X -> else S
X ->
````

## LR Parsing

The weakness of LL(k) parsing techniues is that they must predict wich production to use,
having seen only the first k tokens of the right-hand side.

### LR Parsing Engine

How does the LR parser know when to shift and when to reduce? By using a deterministic finite automaton!
The DFA is not applied to the input - finite automata are too weak to parse context-free grammars -
but to the stack.

### LR(0) Parser Generation

An LR(k) parser uses the content of its stack and the next k tokens of the input to decide which action to take.

### SLR Parser Generation

A simple way to construct better-than-LR(0) parsers is called SLR which stands for Simple LR.
Parser construction for SLR is almost identical to that of LR(0), except that we put reduce actions
into the table only where indicated by the FOLLOW set.

### LR(1) Items; LR(1) Parsing Table

Even more powerful than SLR is the LR(1) parsing algorithm.
Most programming languages whose syntax is describable by a context-free grammar have an LR(1) grammar.

### LALR(1) Parsing Tables

LR(1) parsing tables can be very large, with many states. A smaller table an be made
by merging any two states whose items are identical except for lokahead sets.
The result parser is called LALR(1) parser, for _Look-Ahead LR(1)_.

For some grammars, the LALR(1) table contains reduce-reduce conflicts where the LR(1) table has none,
but in practice the difference matter little.

### Hierarchy of Grammar classes

All SLR grammars are LALR(1), but not vice versa.

Any reasonable programming language has a LALR(1) grammar,
and there are many parser-generator tools available for LALR(1) grammars.
For this reason, LALR(1) has become a standard for programming languages and for automatic parser generators.

### LR parsnig of ambiguous grammars

Many programming languages have grammar rules such has

````
S -> if E then S else S
S -> if E then S
S -> other
````

which allow programs such as

````
if a then if b then s1 else s2
````

Such a program could be understood in two ways:

````
(1) if a then {if b then s1 else s2 }
(2) if a then {if b then s1} else s2
````